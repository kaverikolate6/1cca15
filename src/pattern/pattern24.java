package pattern;

public class pattern24 {
    public static void main(String[] args) {
        int lines=4;
        int star=3;
        int ch=1;
        for(int i=0;i<lines;i++){

            for(int j=0;j<star;j++){
                if(j%2==0){
                    System.out.print("*" +"\t");

                }else{
                    System.out.print(ch +"\t");
                }
            }
            System.out.println();
            ch++;

        }

    }

}



