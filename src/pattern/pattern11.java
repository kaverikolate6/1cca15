package pattern;

public class pattern11 {    public static void main(String[] args) {
    int lines = 5;
    int star = 1;
    char ch1='A';
    for (int i = 0; i < lines; i++) {
        char ch2='A';

        for (int j = 0; j < star; j++) {
            System.out.print(ch2++  +"\t");
        }

        System.out.println();
        star++;
        ch1++;
    }

}

}
