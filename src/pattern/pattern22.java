package pattern;

public class pattern22 {
    public static void main(String[] args) {
        int lines=5;
        int star=5;

        for(int i=0;i<lines;i++){
          int ch=1;
            for(int j=0;j<star;j++){
                if(i%2==0){
                    System.out.print("*" +"\t");

                }else{
                    System.out.print(ch +"\t");
                }
            }
            System.out.println();
            star--;
            ch++;

        }

    }
}
