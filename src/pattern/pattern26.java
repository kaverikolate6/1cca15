package pattern;

public class pattern26 {
    public static void main(String[] args) {
        int lines=3;
        int star=5;
        for(int i=0;i<lines;i++){

            for(int j=0;j<star;j++) {
                if ( j%2==0) {
                    System.out.print("*" +"\t");

                }
            }

            System.out.println();

            star++;
            star+=2;
        }

    }

}



