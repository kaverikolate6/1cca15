package pattern;

public class pattern29 {
    public static void main(String[] args) {
        int lines=7;
        int star=1;

        for(int i=0;i<lines;i++)
        {
            int ch=3;
            for(int j=0;j<star;j++)
            {
                System.out.print(ch--);

            }
            System.out.println();
            if(i<3)
                star++;

            else
                star--;

        }
    }
}
