package pattern;

public class pattern34 {
    public static void main(String[] args) {
        int lines=4;
        int star=1;
        for(int i=0;i<lines;i++)
        {
            int n1=0,n2=1;
            for(int j=0;j<star;j++){
                System.out.print(n1);
                int a=n1+n2;
                n1=n2;
                n2=a;
            }
            System.out.println();
            star+=2;

        }
    }
}
