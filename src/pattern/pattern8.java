package pattern;

public class pattern8 {    public static void main(String[] args) {
    int lines = 5;
    int star = 1;
    int ch1=1;
    for (int i = 0; i < lines; i++) {
        int ch2=1;

        for (int j = 0; j < star; j++) {
            System.out.print("" +ch2++ *ch1+"" +"\t");
        }

        System.out.println();
        star++;
        ch1++;
    }

}

}
