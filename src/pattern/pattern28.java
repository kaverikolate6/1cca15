package pattern;

public class pattern28 {
    public static void main(String[] args) {
        int lines = 4;
        int star = 7;

        for (int i = 0; i < lines; i++) {

            for (int j = 0; j < star; j++) {

                System.out.print(" * ");


            }
            System.out.println();
            if (i<=2) {
                star--;
            } else {
                star++;
            }
        }
    }
}


