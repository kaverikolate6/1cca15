package pattern;

public class pattern31 {
    public static void main(String[] args) {
        int lines = 5;
        int star = 5;
        int ch = 1;
        for (int i = 0; i < lines; i++) {

            int ch1 = ch;
            for (int j = 0; j < star; j++) {
                System.out.print(ch1++);
                if (ch1 > 5)
                    ch1 = 5;
            }

            System.out.println();
            ch++;

        }
    }
}
