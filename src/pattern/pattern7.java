package pattern;

public class pattern7 {public static void main(String[] args) {
    int lines=5;
    int star=5;
    char ch1='A';
    for(int i=0;i<lines;i++) {
        char ch2 = ch1;
        for (int j = 0; j < star; j++) {
            System.out.print(ch2 +"\t");
            ch2 += 1;
        }


        System.out.println();
        ch1++;
    }
}
}


