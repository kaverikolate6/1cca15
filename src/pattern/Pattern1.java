package pattern;

public class Pattern1 {
    public static void main(String[] args) {
        int lines = 5;
        int star = 5;
        int ch = 1;

        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < star; j++) {
                System.out.print(ch++ +"\t");

                if (ch > 25) {
                    ch = 1;
                }
            }


            System.out.println();
        }
    }
}