package test1;

public class test {
    public static void main(String[] args) {
        int lines = 5;
        int star = 1;
        int space = 4;
        char ch1 = 'A';
        for (int i = 0; i < lines; i++) {

            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
                for (int k = 0; k < star; k++) {
                    System.out.print(ch1++);
                }

                System.out.println();
                  star++;
                space--;
            }
        }
    }
