package override;
import java.util.Scanner;
public class MainApp1 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter Duration");
        int duration = sc1.nextInt();
        System.out.println("Enter Fee ");
        int fee = sc1.nextInt();
        System.out.println("Select Courses");
        System.out.println("1:Development \n2:Testing");
        int choice = sc1.nextInt();
        if (choice == 1) {
            Development d1 = new Development();
            d1.displayDetails(duration, fee);
        }
        if (choice == 2) {
            Testing t1 = new Testing();
            t1.displayDetails(duration, fee);
        } else {
            System.out.println("Invalid Choice");
        }
    }
}
